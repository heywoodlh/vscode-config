{
  description = "vscode";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nix-utils.url = "gitlab:kylesferrazza/nix-utils";
  inputs.nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-utils,
    nix-utils,
    nix-vscode-extensions,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      importedPkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      allExtensions = nix-vscode-extensions.extensions.${system};

      mkVSCode = {
        pkgs ? importedPkgs,
        settings,
        extensions,
      }:
        nix-utils.wrapFakeHome {
          inherit pkgs;
          package = pkgs.vscode-with-extensions.override {
            vscodeExtensions = extensions;
          };
          binName = "code";
          files.".config/Code/User/settings.json" = pkgs.writeText "settings.json" (builtins.toJSON settings);
        };

      my-vscode = mkVSCode {
        settings = {
          "workbench.colorTheme" = "Solarized Dark";
        };
        extensions = with allExtensions.vscode-marketplace; [
          vscodevim.vim
        ];
      };
    in {
      packages.default = my-vscode;
      inherit mkVSCode;
      formatter = importedPkgs.alejandra;
    });
}
